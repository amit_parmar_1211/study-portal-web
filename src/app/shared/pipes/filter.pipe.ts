import { Pipe, PipeTransform } from '@angular/core';
import { pipe } from 'rxjs';

@Pipe({
  name: 'filterArr'
})
export class ArrayFilterPipe implements PipeTransform {

  transform(items: any, filter?: any): any {
    if (filter && Array.isArray(items)) {
      const filterKeys = Object.keys(filter);
      return items.filter(item =>
        filterKeys.reduce((memo, keyName) =>
          (memo && new RegExp(filter[keyName], 'gi').test(item[keyName])) || filter[keyName] === '', true));
    } else {
      return items;
    }
  }

}
@Pipe({
  name: 'filterArrV2'
})
export class ArrayFilterV2Pipe implements PipeTransform {

  transform(items: any, filter?: any): any {
    if (filter && Array.isArray(items)) {
      const filterKeys = Object.keys(filter);
      return items.filter(el => el[filterKeys[0]] === filter[filterKeys[0]]);
    } else {
      return items;
    }
  }

}
@Pipe({
  name: 'filterArruniqV3'
})
export class ArrayFilterUniqueV3Pipe implements PipeTransform {

  transform(items: any, filter?: any): any {
    if (filter && Array.isArray(items)) {
      const filterKeys = Object.keys(filter);
      let arrval = items.filter(el => el[filterKeys[0]] === filter[filterKeys[0]]);
      let arrval1 = [], arrval2 = [];
      arrval.forEach(element => {
        if (!arrval1.includes(element.question)) {
          arrval1.push(element.question);
          arrval2.push(element);
        }
      });
      return arrval2;
    } else {
      return items;
    }
  }

}
