import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef, PLATFORM_ID, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EncDecService } from 'src/app/shared/services/enc-dec.service';
import { UniversityService } from '../../services/university.service';
import { LoaderService } from 'src/app/shared/services/message/loader.service';
import { Subject } from 'rxjs';
import { takeUntil, elementAt } from 'rxjs/operators';
import { AuthdataService } from 'src/app/auth/services/authdata.service';
@Component({
  selector: 'app-managemissingfields',
  templateUrl: './managemissingfields.component.html',
  styleUrls: ['./managemissingfields.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ManagemissingfieldsComponent implements OnInit, OnDestroy {
  componentDestroyed$: Subject<boolean> = new Subject();
  option: any;
  count:any;
  unvname = [];
  pageIndex = 1;
  pageSize = 50;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private encdec: EncDecService,
    public uservice: UniversityService,
    public loadService: LoaderService,    
    private cd: ChangeDetectorRef,
    private adataservice: AuthdataService,
    @Inject(PLATFORM_ID) private _platformId: Object,
  ) { }
  permissions: any = {};
  ngOnInit() {
    this.permissions = this.adataservice.getPermission('University Issue');
    this.route.params.subscribe(data => {     
      this.count=this.encdec.convertText('dec',data.count.toString());
       this.option = this.encdec.convertText('dec', data.option.toString());   
       });
       
       this.getuniversity()
  }
  public getuniversity() {
    this.loadService.loadme = true;
    this.uservice.getuniversitybyoption(this.option,this.pageIndex,this.pageSize).subscribe(data => { 
      console.log(data)
      if (data) {
        this.unvname = (data.outdatalist[0] !== "No Data Found") ? JSON.parse(data.outdatalist[0]) : []
        this.loadService.loadme = false;
        this.cd.markForCheck();
      }  
      
    })
  }
  pageChanged(event) {
    this.pageIndex = event;
    this.getuniversity();
    this.cd.markForCheck();
  }
  ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

}
