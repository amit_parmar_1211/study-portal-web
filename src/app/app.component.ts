import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { LoaderService } from './shared/services/message/loader.service';
import { Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { environment } from 'src/environments/environment';
import { JQueryService } from 'src/app/shared/services/message/jQuery.service';
import { SecureDocsService } from './shared/services/securedocs.service';

declare const $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  constructor(
    private router: Router,
    public loadService: LoaderService,
    @Inject(PLATFORM_ID) private _platformId: Object,
    private jqservice: JQueryService,
    private secureDocsService: SecureDocsService,

  ) {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        if (isPlatformBrowser(this._platformId)) {
          $('[data-toggle="tooltip"]').tooltip('dispose');
          // console.clear();
          if (environment.production) {
            if (window) {
              window.console.log = function () { };
            }
          }
        }
      }
    });
  }
  ngOnInit(): void {
    this.loadService.ispanel = false;
    if (!sessionStorage.getItem('smcproductid')) {
      let url = (window.location !== window.parent.location)
        ? document.referrer
        : document.location.href;
      console.log('url : ', this.router);
      const referrerurl = url.match(/:\/\/(.[^/]+)/)[1].trim();
      navigator.geolocation.getCurrentPosition((position) => {
        const lat = position.coords.latitude;
        const lng = position.coords.longitude;
        sessionStorage.setItem('lat', lat.toString());
        sessionStorage.setItem('lng', lng.toString());
      });
      this.secureDocsService.getProductIdUsingReferer(referrerurl).subscribe(data => {
        if (data.flag && data.outdatalist.length > 0) {
          const result = (data.outdatalist[0] && data.outdatalist[0] != 'No Data found') ? data.outdatalist[0][0] : [];
          if (result.projectid) {
            sessionStorage.setItem('smcproductid', result.projectid);
          }
          if (result.apikey && result.apikey != null) {
            sessionStorage.setItem('smcapikey', result.apikey);
          }
        }
      });
    }

  }

  title = 'Searchmycourse.com';
}
