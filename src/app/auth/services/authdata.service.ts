import { Injectable } from '@angular/core';
import { Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { EncDecService } from '../../shared/services/enc-dec.service';
import { Permissions } from 'src/app/models/user.model';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToastService } from 'src/app/shared/services/message/toast.service';
import { Router } from '@angular/router';
import { SMCAuthService } from './auth.service';
declare const moment: any;

@Injectable({
  providedIn: 'root'
})
export class AuthdataService {
  userdata: any;
  tmpdata: any;
  helper = new JwtHelperService();
  constructor(
    @Inject(PLATFORM_ID) private _platformId: Object,
    private encdec: EncDecService,
    private aservice: SMCAuthService,
    private mservice: ToastService,
    private router: Router,
  ) { }
  public setMenu(menu) {
    if (menu) {
      // localStorage.setItem('mdatasmc', this.encdec.convertText('enc', JSON.stringify(menu)));
      localStorage.setItem('mdatasmc', menu);
    } else {
      localStorage.removeItem('mdatasmc');
    }
  }
  public getMenu() {
    // const token1: any = this.getDecodedToken();
    // let url = (window.location !== window.parent.location)
    // //   ? document.referrer
    //   : document.location.href;
    // const referrerurl = url.match(/:\/\/(.[^/]+)/)[1].trim();
    // if (referrerurl !== token1.actort) {
    //   this.router.navigate(['/auth/login']);
    // }
    const menu = (localStorage.getItem('mdatasmc')) ? JSON.parse(this.encdec.convertText('dec', localStorage.getItem('mdatasmc'))) : [];
    return menu.filter(m => m.canbind === true);
  }
  public setPermission(permission) {
    if (permission) {
      localStorage.setItem('pdatasmc', permission);
      // localStorage.setItem('pdatasmc', JSON.stringify(pdata));
    } else {
      localStorage.removeItem('pdatasmc');
    }
  }
  public getPermission(menuname) {
    // const token1: any = this.getDecodedToken();
    // let url = (window.location !== window.parent.location)
    // //   ? document.referrer
    //   : document.location.href;
    // const referrerurl = url.match(/:\/\/(.[^/]+)/)[1].trim();
    // if (referrerurl !== token1.actort) {
    //   this.router.navigate(['/auth/login']);
    // }
    const permissiondata = new Permissions();
    let permissions = (localStorage.getItem('pdatasmc')) ? JSON.parse(this.encdec.convertText('dec', localStorage.getItem('pdatasmc'))) : null;
    if (permissions) {
      const pdata = permissions.filter(p => p.menu === menuname)
      pdata.forEach(element => {
        switch (element.permission.toLowerCase()) {
          case 'view': {
            permissiondata.canView = true;
            break;
          }
          case 'create': {
            permissiondata.canAdd = true;
            break;
          }
          case 'update': {
            permissiondata.canEdit = true;
            break;
          }
          case 'delete': {
            permissiondata.canDelete = true;
            break;
          }
          case 'map': {
            permissiondata.canMap = true;
            break;
          }
          case 'import': {
            permissiondata.canImport = true;
            break;
          }
          case 'export': {
            permissiondata.canExport = true;
            break;
          }
          case 'approve': {
            permissiondata.canApprove = true;
            break;
          }
          case 'reject': {
            permissiondata.canReject = true;
            break;
          }
          case 'share': {
            permissiondata.canShare = true;
            break;
          }
        }
      });
    }
    return permissiondata;
  }

  public getToken(): string {
    if (isPlatformBrowser(this._platformId)) {
      try {
        if (localStorage.getItem('smctoken') !== null && localStorage.getItem('smctoken') !== '') {

          return JSON.parse(this.encdec.convertText('dec', localStorage.getItem('smctoken')));
        } else {
          return null;
        }
      } catch (e) {
        console.log(e);
        return null;
      }
    }
  }

  public setToken(token) {
    if (isPlatformBrowser(this._platformId)) {
      if (token) {
        localStorage.setItem('smctoken', this.encdec.convertText('enc', JSON.stringify(token)));
      } else {
        localStorage.removeItem('smctoken');
      }

    }
  }
  public setAgentInfo(info) {
    if (isPlatformBrowser(this._platformId)) {
      if (info) {
        sessionStorage.setItem('agentinfosmc', this.encdec.convertText('enc', JSON.stringify(info)));
      } else {
        sessionStorage.removeItem('agentinfosmc');
      }

    }
  }
  public getAgentInfo() {
    if (isPlatformBrowser(this._platformId)) {
      if (sessionStorage.getItem('agentinfosmc')) {
        return JSON.parse(this.encdec.convertText('dec', sessionStorage.getItem('agentinfosmc')));
      } else {
        return null;
      }

    }
  }
  public isAuthenticated(): boolean {
    if (isPlatformBrowser(this._platformId)) {
      const token: any = this.getToken();
      if (token && token !== null) {
        try {
          return this.helper.decodeToken(token.token);
        } catch (Error) {
          return false;
        }
      }
      return false;
    }
  }
  public getDecodedToken(): any {
    if (isPlatformBrowser(this._platformId)) {
      const token: any = this.getToken();
      if (token && token !== null) {
        try {
          return this.helper.decodeToken(token.token);
        } catch (Error) {
          return false;
        }
      }
      return false;
    }
  }
  public isTokeExpired(): boolean {
    if (isPlatformBrowser(this._platformId)) {
      const token: any = this.getToken();
      if (token && token !== null) {
        try {
          return this.helper.isTokenExpired(token.token);
        } catch (Error) {
          return true;
        }
      }
      return true;
    }
  }
  public isRefreshTokeExpired(): boolean {
    if (isPlatformBrowser(this._platformId)) {
      const token: any = this.getToken();
      if (token && token !== null) {
        try {
          return this.helper.isTokenExpired(token.refreshtoken);
        } catch (Error) {
          return true;
        }
      }
      return true;
    }
  }
  public getUserId(): any {
    const token: any = this.getToken();
    if (token && token !== null) {
      return this.helper.decodeToken(token.token).nameid;
    }
    return 0;
  }
  public getUserData(): any {
    if (isPlatformBrowser(this._platformId)) {
      const tmpdata = localStorage.getItem('userdata');
      if (tmpdata === '' || tmpdata == null) {
        this.userdata = '';
      } else {
        this.userdata = JSON.parse(this.encdec.convertText('dec', localStorage.getItem('userdata')));
      }
      return this.userdata;
    }
  }
  public setUserData(data): void {
    if (isPlatformBrowser(this._platformId)) {
      if (data) {
        // localStorage.setItem('userdata', this.encdec.convertText('enc', JSON.stringify(data)));
        localStorage.setItem('userdata', data);
      } else {
        localStorage.removeItem('userdata');
      }
    }
  }
  public getNewTicket(rtoken) {
    this.aservice.getNewToken(rtoken).subscribe(data => {
      if (data.flag) {
        this.setToken(data.outdata);
        return this.getToken();
      } else {
        this.setToken('');
        this.setUserData('');
        this.setMenu('');
        this.setPermission('');
        this.mservice.generateMessage('ERROR', 'Session expired!', 'Timeout');
        this.router.navigate(['/auth/login']);
      }
    });
  }

  public getCookieData(): any {
    if (isPlatformBrowser(this._platformId)) {
      let cookie = this.getCookie('_smc_ga');
      if (cookie) {
        return cookie;
      } else {
        cookie = this.createCookie('_smc_ga', '', 3);
        return cookie;
      }
    }
  }
  public getCookie(name) {
    var regexp = new RegExp('(?:^' + name + '|;\s*' + name + ')=(.*?)(?:;|$)', 'g');
    var result = regexp.exec(document.cookie);
    return (result === null) ? null : result[1];
  }
  public createCookie(name, value, hour) {
    var date = new Date();
    date.setTime(date.getTime() + hour * 3600 * 1000);
    var expires = '; expires=' + date;
    var ua = navigator.userAgent.toLowerCase();
    var s;
    (s = ua.match(/msie ([\d.]+)/)) ? value = s[1] :
      (s = ua.match(/firefox\/([\d.]+)/)) ? value = s[1] :
        (s = ua.match(/chrome\/([\d.]+)/)) ? value = s[1] :
          (s = ua.match(/opera.([\d.]+)/)) ? value = s[1] :
            (s = ua.match(/version\/([\d.]+).*safari/)) ? value = s[1] : 0;
    value += value + '_' + date.getTime();
    document.cookie = name + '=' + (value || '') + expires + '; path=/';
    return value;
  }
  public getBrowserDetails() {
    let e = this.init();
    return e;
  }
  public init() {
    let header = [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor];
    let dataos = [
      { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
      { name: 'Windows', value: 'Win', version: 'NT' },
      { name: 'iPhone', value: 'iPhone', version: 'OS' },
      { name: 'iPad', value: 'iPad', version: 'OS' },
      { name: 'Kindle', value: 'Silk', version: 'Silk' },
      { name: 'Android', value: 'Android', version: 'Android' },
      { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
      { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
      { name: 'Macintosh', value: 'Mac', version: 'OS X' },
      { name: 'Linux', value: 'Linux', version: 'rv' },
      { name: 'Palm', value: 'Palm', version: 'PalmOS' }
    ];

    let databrowser = [
      { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
      { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
      { name: 'Safari', value: 'Safari', version: 'Version' },
      { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
      { name: 'Opera', value: 'Opera', version: 'Opera' },
      { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
      { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
    ];
    var agent = header.join(' '),
      os = this.matchItem(agent, dataos),
      browser = this.matchItem(agent, databrowser);

    let region = {
      lat: 0,
      long: 0
    }
    region.long = parseFloat(sessionStorage.getItem('lng'));
    region.lat = parseFloat(sessionStorage.getItem('lat'));
    let device = this.getDeviceType();
    return { os: os, browser: browser, region, device };
  }
  public getDeviceType() {
    const ua = navigator.userAgent;
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      return 'tablet';
    }
    if (
      /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        ua
      )
    ) {
      return 'mobile';
    }
    return 'desktop';
  }
  public matchItem(string, data) {
    var i = 0,
      j = 0,
      html = '',
      regex,
      regexv,
      match,
      matches,
      version;

    for (i = 0; i < data.length; i += 1) {
      regex = new RegExp(data[i].value, 'i');
      match = regex.test(string);
      if (match) {
        regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
        matches = string.match(regexv);
        version = '';
        if (matches) { if (matches[1]) { matches = matches[1]; } }
        if (matches) {
          matches = matches.split(/[._]+/);
          for (j = 0; j < matches.length; j += 1) {
            if (j === 0) {
              version += matches[j] + '.';
            } else {
              version += matches[j];
            }
          }
        } else {
          version = '0';
        }
        return {
          name: data[i].name,
          version: parseFloat(version)
        };
      }
    }
    return { name: 'unknown', version: 0 };
  }
}
