import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagemissingfieldsComponent } from './managemissingfields.component';

describe('ManagemissingfieldsComponent', () => {
  let component: ManagemissingfieldsComponent;
  let fixture: ComponentFixture<ManagemissingfieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagemissingfieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagemissingfieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
