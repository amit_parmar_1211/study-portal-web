import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { EncDecService } from 'src/app/shared/services/enc-dec.service';

@Injectable({
  providedIn: 'root'
})
export class CourseIssueService {
  prefix: string = environment.API_URL;
  suffix: string;
  constructor(
    private httpclient: HttpClient,
    private encdec: EncDecService
  ) { }

  public getCourse(countryid,universityid, studylevelid, pageindex, pagesize, issuetype,disciplineid,substudylevelid,searchstring): Observable<any> {
 
    const objparams = {
      
      countryid:this.encdec.encryptSensitive(countryid),
      universityid: this.encdec.encryptSensitive(universityid),
      studylevelid: this.encdec.encryptSensitive(studylevelid),
      issuetype: this.encdec.encryptSensitive(issuetype), 
      disciplineid:this.encdec.encryptSensitive(disciplineid), 
      substudylevelid:this.encdec.encryptSensitive(substudylevelid),   
      pageindex,
      pagesize,
      searchstring
     
      
    }
    return this.httpclient.post<any>(`${this.prefix}api/courseCriteria/v1/getCourseforissue`, objparams, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
  public getCourseforexcel(universityid, studylevelid, issuetype): Observable<any> {

    const objparams = {
      universityid: this.encdec.encryptSensitive(universityid),
      studylevelid: this.encdec.encryptSensitive(studylevelid),
      issuetype: this.encdec.encryptSensitive(issuetype)
    }
    return this.httpclient.post<any>(`${this.prefix}api/courseCriteria/v1/getCourseforexcel`, objparams, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
  public getCoursebyId(courseid): Observable<any> {
    return this.httpclient.get<any>(`${this.prefix}api/courseCriteria/v1/getCourse?courseid=${courseid}`, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
  public saveCourse(coursedata, userid): Observable<any> {
    const payload = {
      coursedata, userid
    };
    return this.httpclient.post<any>(`${this.prefix}courses/au/v2/saveCourse`, payload);
  }

  public globlahints(searchtext, pageindex, pagesize, universityid, searchtype): Observable<any> {
    return this.httpclient.get<any>(`${this.prefix}courses/au/v1/globalhints?searchtext=${searchtext}&pageindex=${pageindex}&pagesize=${pagesize}&universityid=${universityid}&searchtype=${searchtype}`, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
  public getAllcourses(searchstring): Observable<any> {

    return this.httpclient.get<any>(`${this.prefix}api/courseCriteria/v1/getcoursesearch?searchstring=${searchstring}`, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }

  public deactivateCourse(courseid,active):Observable<any>{
   var  courseid =this.encdec.encryptSensitive(courseid);
   var isactives=this.encdec.encryptSensitive(active);
   return this.httpclient.post<any>(`${this.prefix}courses/au/v1/deactivecourse?courseid=${courseid}&isactives=${isactives}`, {
     headers:{
       'Content-Type':'application/json'
     }
   })

  }
}
