import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef, PLATFORM_ID, Inject } from '@angular/core';
import { UniversityService} from '../../services/university.service';
import { LoaderService } from 'src/app/shared/services/message/loader.service';
import { Subject } from 'rxjs';
import { takeUntil, elementAt } from 'rxjs/operators';
declare const $: any;

@Component({
  selector: 'app-missinguniversityfieldreport',
  templateUrl: './missinguniversityfieldreport.component.html',
  styleUrls: ['./missinguniversityfieldreport.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MissinguniversityfieldreportComponent implements OnInit,OnDestroy {
  componentDestroyed$: Subject<boolean> = new Subject();
totaluniversity=0;
mpostcodecnt=0;
mcountrycnt=0;
mcitycnt=0;
mstatecnt=0;
mcricoscnt=0;
mvtour=0;
mfbcnt=0;
myoutubecnt=0;
mtwittercnt=0;
mabbcnt=0;
mappfee=0;
munvtypecnt=0;
mappurlcnt=0;
mplacementurlcnt=0;
maccomodationcnt=0;
mplaceid=0;

potscodeper:any
countryper:any
cityper:any
stateper:any
cricosper:any
vtourper:any
fbpageper:any
youtubepageper:any;
twitterpageper:any;
abbper:any;
appfeeper:any;
unvtypeper:any;
appurlper:any;
placementurlper:any;
accomodationper:any;
placeidper:any;
unvname=[];
header:any;
  constructor(
    public uservice:UniversityService,
    public loadService: LoaderService,
    private cd: ChangeDetectorRef,
    @Inject(PLATFORM_ID) private _platformId: Object,
  ) { }

  ngOnInit() {

    this.getmissingfieldscount()
  }
  public getmissingfieldscount(){
    this.loadService.loadme = true;
    this.uservice.getcount().pipe(takeUntil(this.componentDestroyed$)).subscribe(data=>{
     if(data){
      this.loadService.loadme = false;
      var totaluniversitycnt=JSON.parse(data.outdatalist[0]);
      this.totaluniversity=totaluniversitycnt[0].totaluniversity
      var cntpostcode=JSON.parse(data.outdatalist[1]);
      this.mpostcodecnt=cntpostcode[0].postcodecnt;
      this.potscodeper=this.getpercantage(this.totaluniversity,this.mpostcodecnt);      
      var  cntcountry=JSON.parse(data.outdatalist[2]);
      this.mcountrycnt=cntcountry[0].countrycnt;
      this.countryper=this.getpercantage(this.totaluniversity,this.mcountrycnt)
      var cntcity=JSON.parse(data.outdatalist[3])
      this.mcitycnt=cntcity[0].citycnt;
      this.cityper=this.getpercantage(this.totaluniversity,this.mcitycnt);
      var cntstate=JSON.parse(data.outdatalist[4]);
      this.mstatecnt=cntstate[0].statecnt;
      this.stateper=this.getpercantage(this.totaluniversity,this.mstatecnt);
      var cntcricos=JSON.parse(data.outdatalist[5]);
      this.mcricoscnt=cntcricos[0].cricosprovidercnt
      this.cricosper=this.getpercantage(this.totaluniversity,this.mcricoscnt);
      var vtourcnt=JSON.parse(data.outdatalist[6]);
      this.mvtour=vtourcnt[0].vtourcnt
      this.vtourper=this.getpercantage(this.totaluniversity,this.mvtour);
      var cntfb=JSON.parse(data.outdatalist[7]);
      this.mfbcnt=cntfb[0].fbpagecnt;
      this.fbpageper=this.getpercantage(this.totaluniversity,this.mfbcnt);
      var cntyoutube=JSON.parse(data.outdatalist[8]);
      this.myoutubecnt=cntyoutube[0].youtubecnt;
      this.youtubepageper=this.getpercantage(this.totaluniversity,this.myoutubecnt);
      var cnttwitter=JSON.parse(data.outdatalist[9]);
      this.mtwittercnt=cnttwitter[0].twittercnt;
      this.twitterpageper=this.getpercantage(this.totaluniversity,this.mtwittercnt);
      var abbcnt=JSON.parse(data.outdatalist[10]);
      this.mabbcnt=abbcnt[0].abbcnt;
      this.abbper=this.getpercantage(this.totaluniversity,this.mabbcnt);
      var appfeecnt=JSON.parse(data.outdatalist[11]);
      this.mappfee=appfeecnt[0].feecnt;
      this.appfeeper=this.getpercantage(this.totaluniversity,this.mappfee);
      var unvtypecnt=JSON.parse(data.outdatalist[12]);
      this.munvtypecnt=unvtypecnt[0].unvtypecnt
      this.unvtypeper=this.getpercantage(this.totaluniversity,this.munvtypecnt);
      var appurlcnt=JSON.parse(data.outdatalist[13]);
      this.mappurlcnt=appurlcnt[0].appurlcnt
      this.appurlper=this.getpercantage(this.totaluniversity,this.mappurlcnt);
      var placementurlcnt=JSON.parse(data.outdatalist[14]);
      this.mplacementurlcnt=placementurlcnt[0].placementurlcnt;
      this.placementurlper=this.getpercantage(this.totaluniversity,this.mplacementurlcnt);
      var accocnt=JSON.parse(data.outdatalist[15]);
      this.maccomodationcnt=accocnt[0].accomodationurlcnt
      this.accomodationper=this.getpercantage(this.totaluniversity,this.maccomodationcnt);
      var cntplaceid=JSON.parse(data.outdatalist[16]);
      this.mplaceid=cntplaceid[0].placeidcnt;
      this.placeidper=this.getpercantage(this.totaluniversity,this.mplaceid)
      this.cd.markForCheck();

     }
     
     // this.mpostcodecnt=JSON.parse(data.outdatalist[1]);
     // console.log(this.totaluniversity)
    })

  }
  public getpercantage(totalunivcnt,cnt){
    return (cnt*100)/totalunivcnt;
   
  }
  ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  

}
