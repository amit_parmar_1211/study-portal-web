import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MstudylevelComponent } from './components/mstudylevel/mstudylevel.component';
import { McurrencyComponent } from './components/mcurrency/mcurrency.component';
import { MdisciplineComponent } from './components/mdiscipline/mdiscipline.component';
import { MapCurrencyComponent } from './components/map-currency/map-currency.component';
import { AuthGuardService } from 'src/app/auth/services/guards/auth-guard.service';
import { McampuslocationComponent } from './components/mcampuslocation/mcampuslocation.component';
import { UniversityissueComponent } from './components/universityissue/universityissue.component';
import { EdituniversityissueComponent } from './components/edituniversityissue/edituniversityissue.component';
import { MissinguniversityfieldreportComponent } from './components/missinguniversityfieldreport/missinguniversityfieldreport.component';
import { CustomAuthGuardService } from 'src/app/auth/services/guards/customauth-guard.service';
import {ManagemissingfieldsComponent} from './components/managemissingfields/managemissingfields.component';
import { CourseIssueComponent } from './components/course-issue/course-issue.component';
import { CourseEditComponent } from './components/course-edit/course-edit.component';
const routes: Routes = [
  {
    path: 'manage-studylevel',
    component: MstudylevelComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'manage-currency',
    component: McurrencyComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'manage-discipline',
    component: MdisciplineComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'manage-country',
    component: MapCurrencyComponent,
    canActivate: [AuthGuardService],
  },
  // {
  //   path: 'manage-university',
  //   component: McampuslocationComponent,
  //   canActivate: [AuthGuardService],
  // },
  {
    path: 'manage-university',
    component: UniversityissueComponent,
    canActivate: [AuthGuardService],
  },
  {
    path:'manage-university/edituniversity/:universityid',
    component:EdituniversityissueComponent,
    canActivate:[CustomAuthGuardService]

  },
  {
    path:'manage-missing-university-field',
    component:MissinguniversityfieldreportComponent,
    canActivate: [AuthGuardService],
  },
  {
    path:'manage-missing-university-field/managemissingfield/:option/:count',
    component:ManagemissingfieldsComponent,
    canActivate:[CustomAuthGuardService]

  },
  {
    path: 'manage-course',
    component: CourseIssueComponent,
    canActivate: [AuthGuardService],
  },
  {
    path:'manage-course/edit-course/:courseid',
    component: CourseEditComponent,
    canActivate:[CustomAuthGuardService]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageRoutingModule { }
