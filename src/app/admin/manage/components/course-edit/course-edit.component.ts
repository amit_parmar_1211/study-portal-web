import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ToastService } from 'src/app/shared/services/message/toast.service';
import { LoaderService } from 'src/app/shared/services/message/loader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthdataService } from 'src/app/auth/services/authdata.service';
import { EncDecService } from 'src/app/shared/services/enc-dec.service';
import { CourseIssueService } from '../../services/course-issue.service';
import { element } from 'protractor';
declare const $: any;

@Component({
  selector: 'app-course-edit',
  templateUrl: './course-edit.component.html',
  styleUrls: ['./course-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CourseEditComponent implements OnInit, OnDestroy {
  permissions: any;
  courseid = 0;
  coursedata: any;
  studylevellist = [];
  disciplinelist = [];
  countrylist = [];
  universityname: any;
  code: any;
  intakevalue: any;

  fees = [];
  feesunit: any;
  radioSelected: any;
  tmpcampus: any;
  tmpfee: any;
  tmpintake: any
  englishrequirements: [];
  description: any;
  name: any;
  speaking: any;
  writing: any;
  listening: any;
  reading: any;
  overrall: any;
  applicationfee:any;
  componentDestroyed$: Subject<boolean> = new Subject();
  constructor(
    private adataservice: AuthdataService,
    //private uservice: UniversityService,
    public loadService: LoaderService,
    private mservice: ToastService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private encdec: EncDecService,
    private cservice: CourseIssueService,
  ) { }

  ngOnInit() {
    this.route.params.pipe(takeUntil(this.componentDestroyed$)).subscribe(params => {
      this.courseid = this.encdec.convertText('dec', params.courseid.toString());
      this.cd.markForCheck();     
    });
    this.cservice.getCoursebyId(this.courseid).pipe(takeUntil(this.componentDestroyed$)).subscribe(data => {
      if (data.flag) {       
        this.coursedata = JSON.parse(data.outdatalist[0][0].coursedata); 
        if(this.coursedata.application_fee){
          this.applicationfee=this.coursedata.application_fee
        } else{
          this.applicationfee=this.coursedata.application_fees
        }
        this.radioSelected = (this.coursedata.course_campus_location.length > 0) ? this.coursedata.course_campus_location[0].name : '';
        this.tmpcampus = this.coursedata.course_campus_location.filter(val => {
          return val.name.toLowerCase() == this.radioSelected.toLowerCase();
        });
        this.tmpfee = this.coursedata.course_tuition_fee.fees.filter(val => {
          return val.name.toLowerCase() == this.radioSelected.toLowerCase();
        })
        this.tmpintake = this.coursedata.course_intake.intake.filter(val => {
          return val.name.toLowerCase() == this.radioSelected.toLowerCase();
        })
        if (this.tmpintake.length == 0) {
          this.tmpintake.push({ name: '', value: [] });
        }        
        this.universityname = data.outdatalist[1][0].name;
        this.studylevellist = data.outdatalist[2];       
        this.disciplinelist = data.outdatalist[3];
        this.countrylist = data.outdatalist[4];
        this.loadService.loadme = false;
        this.cd.markForCheck();
      } else {
        this.loadService.loadme = false;
        this.mservice.generateMessage('ERROR', 'FAILED', data.message);
      }
    });
  }
  public trackByIndex(index: number) {
    return index;
  }
  addOutcome() {
    this.coursedata.course_career_outcome.push('');
    this.cd.markForCheck();
  }
  removeOutcome(index) {
    this.coursedata.course_career_outcome.splice(index, 1);
    this.cd.markForCheck();
  }
  addDiscipline() {
    this.coursedata.course_discipline.push('');
    this.cd.markForCheck();
  }
  removeDiscipline(index) {
    this.coursedata.course_discipline.splice(index, 1);
    this.cd.markForCheck();
  }


  findindex(campus) {
    var nm = this.coursedata.course_intake.intake
    return nm.findIndex(x => x.name === campus);
    //console.log(nm.findIndex(x=>x.name===campus))
  }
  addSubIntake(campus) {
    const subinatake: any = {};
    subinatake.actualdate = '';
    subinatake.month = '';
    var i = this.findindex(campus);
    this.coursedata.course_intake.intake[i].value.push(subinatake);
    this.cd.markForCheck();
  }
  removeSubIntake(subindex, campus) {
    var i = this.findindex(campus);
    this.coursedata.course_intake.intake[i].value.splice(subindex, 1);
    this.cd.markForCheck();
  }
  addDuration() {
    const duration: any = {};
    duration.unit = '';
    duration.duration = '';
    duration.filterduration = '';
    duration.display = '';
    this.coursedata.course_duration_display.push(duration);
    this.cd.markForCheck();
  }
  removeDuration(index) {
    this.coursedata.course_duration_display.splice(index, 1);
    this.cd.markForCheck();
  }
  onItemChange(campus) {
    this.tmpcampus = this.coursedata.course_campus_location.filter(val => {
      return val.name.toLowerCase() === campus.toLowerCase();
    });
    this.tmpfee = this.coursedata.course_tuition_fee.fees.filter(val => {
      return val.name.toLowerCase() === campus.toLowerCase();
    });
    this.tmpintake = this.coursedata.course_intake.intake.filter(val => {
      return val.name.toLowerCase() === campus.toLowerCase();
    });
  }

  saveDetails(tmplocation, tmpfees, tmpintakes) {

    this.coursedata.course_campus_location.forEach(element => {
      if (element.name == tmplocation[0].name) {
        element.code = tmplocation[0].code;
      }

    });
    this.coursedata.course_tuition_fee.fees.forEach(element => {
      if (element.name == tmpfees[0].name) {
        element.value = tmpfees[0].value;
      }
    });
    this.coursedata.course_intake.intake.forEach(element => {
      if (element.name == tmpintakes[0].name) {
        element.value = tmpintakes[0].value;
      }
    })    ;
  }
  opencontactinfoModal(name) {
    $('#myModal').modal('toggle');
    // console.log(this.coursedata.course_admission_requirement.english)
    this.coursedata.course_admission_requirement.english.forEach(element => {
      if (element.name == name) {
        this.name = element.name;
        this.description = element.description;
        this.reading = element.R;
        this.writing = element.W;
        this.speaking = element.S;
        this.listening = element.L;
        this.overrall = element.O;

      }
    })

    //this.englishrequirements=this.coursedata.course_admission_requirement.english
  }
  save_english() {    
    this.coursedata.course_admission_requirement.english.forEach(element => {
      if (element.name == this.name) {

        element.description = this.description;
        element.R = this.reading;
        element.W = this.writing;
        element.S = this.speaking;
        element.L = this.listening;
        element.O = this.overrall;
      }
    })
    this.mservice.generateMessage('SUCCESS', 'English Score Updated Successfully', 'SUCCESS');   
    $("#myModal").modal('hide');
  }
  clearall() {
    $("#myModal").modal('hide');

  }
  saveCourse(applicationfee){
    //console.log(this.coursedata)
    this.loadService.loadme = true;
    if(this.coursedata.application_fee){
     this.coursedata.application_fee=applicationfee
    } else{
      this.coursedata.application_fees=applicationfee
    }

    this.cservice.saveCourse([this.coursedata], this.adataservice.getUserId()).pipe(takeUntil(this.componentDestroyed$)).subscribe(data => {
      if (data.flag) {
        this.loadService.loadme = false;
        this.mservice.generateMessage('SUCCESS', 'Course updated successfully.', 'SUCCESS');
        this.cd.markForCheck();
      } else {
        this.loadService.loadme = false;
        this.mservice.generateMessage('ERROR', data.message, 'FAILED');
      }
    });

  }


  ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
