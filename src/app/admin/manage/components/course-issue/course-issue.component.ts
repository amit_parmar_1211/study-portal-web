import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';

import { ToastService } from 'src/app/shared/services/message/toast.service';
import { LoaderService } from 'src/app/shared/services/message/loader.service';
import { CourseIssueService } from '../../services/course-issue.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthdataService } from 'src/app/auth/services/authdata.service';
import * as XLSX from 'xlsx';
import { CourseIssueExcelService } from '../../services/courseissueexcel.service';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType } from '@angular/common/http';
import { EncDecService } from 'src/app/shared/services/enc-dec.service';
import { environment } from 'src/environments/environment';
import { Searchhints } from '../../models/coursesearch.model';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

import { Inject, PLATFORM_ID } from '@angular/core';
declare const $: any;
@Component({
  selector: 'app-course-issue',
  templateUrl: './course-issue.component.html',
  styleUrls: ['./course-issue.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CourseIssueComponent implements OnInit, OnDestroy {
  componentDestroyed$: Subject<boolean> = new Subject();
  universityid = 0;
  studylevelid = 0;
  countryid = 0;
  disciplineid = 0;
  issuetype = 0;
  substudylevelid =0;
  pageIndex = 1;
  pageSize = 50;
  courselist = [];
  universitylist = [];
  studylevellist = [];
  countrylist = [];
  disciplinelist = [];
  substudylevel=[];
  coursecount = 0;
  prepare = false;
  download = false;
  downloadpr = 0;
  totalsize = 0;
  downloadedsize = 0;
  speed = '';
  searchhints = new Searchhints();
  isbuttonclicke = false;
  escButton = false;
  divFocusFlag = false;
  inputFocusFlag = false;
  searchstring = '';
  issuesearchstring=''
  coursecnt = 0; 

  constructor(
    private cservice: CourseIssueService,
    public loadService: LoaderService,
    private mservice: ToastService,
    private cd: ChangeDetectorRef,
    private adataservice: AuthdataService,
    private excelService: CourseIssueExcelService,
    private encdec: EncDecService,
    private httpclient: HttpClient,
    @Inject(PLATFORM_ID) private _platformId: Object,
  ) { }
  permissions: any = {};
  permission: any = {};
  ngOnInit() {
    this.permissions = this.adataservice.getPermission('Course Issue');
    this.permission = this.adataservice.getPermission('Course Edit');
    this.getAllCourses();
  }
  getAllCourses() {
    this.loadService.loadme = true;
   
    this.cservice.getCourse(this.countryid, this.universityid, this.studylevelid,
      this.pageIndex, this.pageSize, this.issuetype, this.disciplineid,this.substudylevelid ,this.searchstring).pipe(takeUntil(this.componentDestroyed$)).subscribe(data => {
         if (data.flag) {
          this.courselist = data.outdatalist[0];
          this.coursecount = data.outdatalist[1][0].coursecount;
          this.universitylist = data.outdatalist[2];
          this.studylevellist = data.outdatalist[3];
          this.countrylist = data.outdatalist[4];
          this.disciplinelist = data.outdatalist[5];
          this.substudylevel=data.outdatalist[6];
          this.loadService.loadme = false;          
          this.cd.markForCheck();
        } else {
          this.loadService.loadme = false;
          this.mservice.generateMessage('ERROR', 'FAILED', data.message);
        }
      });
      
  }
  deactive(courseid, isactive) {
    var active = (isactive == 1) ? 0 : 1;
    this.cservice.deactivateCourse(courseid, active).pipe(takeUntil(this.componentDestroyed$)).subscribe(data => {
      if (data.flag) {
        this.mservice.generateMessage('SUCCESS', data.message, 'SUCCESS');
        this.getAllCourses();
      } else {
        this.mservice.generateMessage('ERROR', 'FAILED', data.message);
      }
    })
  }
  getCourseByFilter() {
    this.isbuttonclicke = true;
    this.loadService.loadme = true;
    this.searchhints = new Searchhints();
    this.getAllCourses();

  }
  public trackByIndex(index: number) {
    return index;
  }
  getCourseByHint(cname) {
    this.searchstring = cname;    
    this.getCourseByFilter();
  }
 globalhints (e) {
    this.divFocusFlag = true;
    if (isPlatformBrowser(this._platformId)) {
      $('#hint').show();
    }
    if (isPlatformBrowser(this._platformId)) {
      this.isbuttonclicke = false;

      if (e.which === 27) {
        this.isbuttonclicke = true;
      } else {
        // if (this.courseFilterModel.searchstring.length > 2 || this.courseFilterModel.searchstring.length === 0) {
        if (this.searchstring.length > 2) {
          if (e.which === 13 || this.searchstring.length === 0) {
            this.getCourseByFilter();
          } else if (e.which !== 40 && e.which !== 38 && e.which !== 37 && e.which !== 39) {
            this.cservice.globlahints(this.searchstring, this.searchhints.pageindex,
              this.searchhints.pagesize, 0, 'course').pipe(takeUntil(this.componentDestroyed$)).subscribe(data => {
                if (data.flag && data.outdatalist[0] !== 'No Data Found') {
                  this.searchhints.coursehint = JSON.parse(data.outdatalist[0]);
                  
                  // this.encyptcourseid(JSON.parse(data.outdatalist[0]));
                  this.searchhints.coursehintcount = JSON.parse(data.outdatalist[2])[0].coursecount;
                  if (this.searchhints.coursehintcount >= (this.searchhints.pageindex * this.searchhints.pagesize)) {
                    this.searchhints.ismorepage = true;
                  } else {
                    this.searchhints.ismorepage = false;
                  }
                  this.cd.markForCheck();
                  setTimeout(() => {
                    $('#list li:first-child').addClass('active');
                  }, 100);
                } else {
                  this.searchhints = new Searchhints();
                }
              });
          } else if (e.which === 40 || e.which === 38) {
            const current = $('#list li.active');
            switch (e.which) {
              case 38:
                $('#list li.active').prev().addClass('active');
                current.removeClass('active');
                break;
              case 40:
                $('#list li.active').next().addClass('active');
                current.removeClass('active');
                break;
            }
            const container = $('#list');
            const scrollTo = $('#list li.active');
            container.animate({
              scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
            }, 100);
            // $('ul').animate({ scrollTop: $('ul li.active').position().top }, 'slow');
          }
        } else {
          this.searchhints = new Searchhints();
        }
      }
    }
  }


  getNextHint() {
    this.isbuttonclicke = false;
    this.divFocusFlag = false;
    if (this.searchhints.ismorepage) {
      this.searchhints.pageindex = this.searchhints.pageindex + 1;
      this.cservice.globlahints(this.searchstring, this.searchhints.pageindex,
        this.searchhints.pagesize, 0, 'course').pipe(takeUntil(this.componentDestroyed$)).subscribe(data => {
          if (data.flag && data.outdatalist[0]) {
            this.searchhints.coursehint = this.searchhints.coursehint.concat(JSON.parse(data.outdatalist[0]));
            this.searchhints.coursehintcount = JSON.parse(data.outdatalist[2])[0].coursecount;
            if (this.searchhints.coursehintcount >= (this.searchhints.pageindex * this.searchhints.pagesize)) {
              this.searchhints.ismorepage = true;
            } else {
              this.searchhints.ismorepage = false;
            }
            this.cd.markForCheck();
          } else {
            this.mservice.generateMessage('ERROR', 'FAILED', data.message);
          }
        });
    }
  }
  pageChanged(event) {
    this.pageIndex = event;
    this.getAllCourses();
    this.cd.markForCheck();
  }
  generateExcel() {
    let coursedata = [];   
   // console.log(this.searchstring);
    const objparams = {
      universityid: this.encdec.encryptSensitive(this.universityid),
      studylevelid: this.encdec.encryptSensitive(this.studylevelid),
      issuetype: this.encdec.encryptSensitive(this.issuetype),
      substudylevelid:this.encdec.encryptSensitive(this.substudylevelid),
      searchstring:this.encdec.encryptSensitive(this.searchstring)
    }
    const req = new HttpRequest('POST', `${environment.API_URL}api/courseCriteria/v1/getCourseforexcel`,
      objparams, {
      reportProgress: true
    });
    this.httpclient.request(req).subscribe((event: HttpEvent<any>) => {
      this.prepare = true;
      $('#progress').modal({ backdrop: 'static', keyboard: false, show: true });
      switch (event.type) {
        case HttpEventType.DownloadProgress:
          this.download = true;
          this.prepare = false;
          this.totalsize = event.total;
          this.speed = (Math.round((event.loaded - this.downloadedsize) / 1024) < 1024) ? Math.round((event.loaded - this.downloadedsize) / 1024) + '/kbps' : Math.round(((event.loaded - this.downloadedsize) / 1024) / 1024) + '/mbps';
          this.downloadedsize = event.loaded;
          this.totalsize = event.total;
          this.downloadpr = Math.round(100 * event.loaded / event.total);
          this.cd.markForCheck();
          break;
        case HttpEventType.Response:
          this.loadService.downloadPDF = false;
          this.download = false;
          this.prepare = false;
          this.downloadpr = 0;
          $('#progress').modal('hide');
          this.cd.markForCheck();
          let data = event.body;
          console.log(data)
          if (data.flag) {
            coursedata = (data.outdatalist[0] !== 'No Data Found') ? JSON.parse(data.outdatalist[0]) : [];
            //console.log(coursedata)
            this.cd.markForCheck();
            this.excelService.generateExcelForCoursedetail(coursedata);
          }
      }
    });
  }
  generateExcelSample() {
    this.excelService.generateExcelForCourseSample();
  }

  importExcel(event) {
    this.prepare = true;
    const filename = event.target.files[0].name;
    const fileExtention = (filename.lastIndexOf('.') > 0) ? filename.substring(filename.lastIndexOf('.') + 1, filename.length) : '';
    if (fileExtention === 'xlsx' || fileExtention === 'xls') {
      const reader = new FileReader();
      const file = event.target.files[0];
      reader.onload = (event) => {
        const data = reader.result;
        const workBook = XLSX.read(data, { type: 'binary' });
        const jsonData = workBook.SheetNames.reduce((initial, name) => {
          const sheet = workBook.Sheets[name];
          initial[name] = XLSX.utils.sheet_to_json(sheet);
          return initial;
        }, {});
        this.excelService.importExcelForCourse(jsonData).subscribe((event: HttpEvent<any>) => {
          $('#progress').modal({ backdrop: 'static', keyboard: false, show: true });
          switch (event.type) {
            case HttpEventType.UploadProgress:
              this.download = true;
              this.prepare = false;
              this.totalsize = event.total;
              this.speed = (Math.round((event.loaded - this.downloadedsize) / 1024) < 1024) ? Math.round((event.loaded - this.downloadedsize) / 1024) + '/kbps' : Math.round(((event.loaded - this.downloadedsize) / 1024) / 1024) + '/mbps';
              this.downloadedsize = event.loaded;
              this.totalsize = event.total;
              this.downloadpr = Math.round(100 * event.loaded / event.total);
              this.cd.markForCheck();
              break;
            case HttpEventType.Response:
              this.loadService.downloadPDF = false;
              this.download = false;
              this.prepare = false;
              this.downloadpr = 0;
              $('#progress').modal('hide');
              this.cd.markForCheck();
              let data = event.body;
              if (data.flag) {
                this.mservice.generateMessage('SUCCESS', 'Course updated successfully.', 'SUCCESS');
              } else {
                this.mservice.generateMessage('ERROR', data.message, 'FAILED');
              }
              break;
          }
        });
        const file: any = document.getElementById('upload-photo');
        file.value = null;
      };
      reader.readAsBinaryString(file);
    } else {
      this.mservice.generateMessage('ERROR', 'Please provide valid file.', 'Extention issue');
      const file: any = document.getElementById('upload-photo');
      file.value = null;
    }
  }
  
  ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
