import { Component, OnInit, ChangeDetectorRef, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { AddressService } from '../../services/address.service';
import { EncDecService } from 'src/app/shared/services/enc-dec.service';
import { ToastService } from 'src/app/shared/services/message/toast.service';
import $ from 'jquery';
import { AuthdataService } from 'src/app/auth/services/authdata.service';
@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddressComponent implements OnInit {
  @Input() allsections;
  @Input() mastercontroll;
  @Output() copyAnswers = new EventEmitter();
  constructor(
    private addressService: AddressService,
    private encdec: EncDecService,
    private cd: ChangeDetectorRef,
    private mservice: ToastService,
    private adataservice: AuthdataService,
  ) { }
  projectid: any;
  leadid: any;
  addressList: any = [];
  divautofillview = true;
  divautofilladd = false;
  divautofillbutton = false;
  userdata: any = {};
  editflag = false;
  autofilljson: any = {
    leadaddline1: '',
    leadaddline2: '',
    leadcountry: '',
    leadstate: '',
    leadcity: '',
    leadpostcode: '',
    leadcontactnumber: '',
    leademail: '',
    isautofill: false,
    projectid: '',
    leadid: '',
    createdby: ''
  };
  sectionlist = [];
  ngOnInit() {
    this.projectid = sessionStorage.getItem('smcproductid');
    this.leadid = (sessionStorage.getItem('kondeskleadid')) ?
      this.encdec.decryptSensitiveV1(sessionStorage.getItem('kondeskleadid')) : (sessionStorage.getItem('aid')) ? sessionStorage.getItem('aid') : undefined;
    this.userdata = (this.adataservice.getUserData() !== ''
      && this.adataservice.getUserData() !== null) ? (this.adataservice.getUserData()[0]) : '';
    this.autofilljson.createdby = this.userdata.email;
    this.autofilljson.projectid = this.projectid;
    this.autofilljson.leadid = this.leadid;

    this.sectionlist = this.allsections.filter(cc => cc.sectionparentidf === 2);
    this.getAutoFillJson();
  }
  getAutoFillJson() {
    this.addressService.getAutoFillDetails(this.projectid, this.leadid).subscribe(data => {
      if (data.flag) {
        this.addressList = (data.outdata && data.outdata !== 'No Data Found') ? data.outdata : [];
        this.setCheckBoxValues();
      }
      this.cd.markForCheck();
    });
  }
  setCheckBoxValues() {
    $('.addresscutom').prop('checked', false);
    $('.addresscutom').prop('disabled', false);
    this.addressList.forEach(element => {
      if (element.copyjson == null || element.copyjson.length == 0) {
        element.copyjson = [];
      } else {
        if (typeof element.copyjson == 'string') {
          element.copyjson = JSON.parse(element.copyjson);
        }
        for (let val of element.copyjson) {
          setTimeout(() => {
            $('#' + val).prop('checked', true);
          }, 100);
          let sectionidf = parseInt(val.split('_')[0]);
          let listid = parseInt(val.split('_')[1]);
          for (let sval of this.addressList) {
            if (sval.id != listid) {
              setTimeout(() => {
                $('#' + sectionidf + '_' + sval.id).prop('disabled', true);
              }, 100);
            }
          }
        }
      }
    });
    this.cd.markForCheck();
  }
  checkValueChanged(json, list) {
    let newJson = [];
    if ($('#' + json.sectionidf + '_' + list.id).prop('checked')) {
      if (list.copyjson == null || list.copyjson.length == 0) {
        newJson.push(json.sectionidf + '_' + list.id);
      } else {
        list.copyjson.push(json.sectionidf + '_' + list.id);
        newJson = list.copyjson;
      }
      for (let val of this.addressList) {
        if (val.id != list.id) {
          $('#' + json.sectionidf + '_' + val.id).attr('disabled', true);
        }
      }
    } else {
      if (list.copyjson != null && list.copyjson.length > 0) {
        const index = list.copyjson.map((e) => e.id).indexOf(json.sectionidf + '_' + list.id);
        list.copyjson.splice(index, 1);
        newJson = list.copyjson;
      }
      for (let val of this.addressList) {
        if (val.id != list.id) {
          $('#' + json.sectionidf + '_' + val.id).attr('disabled', false);
        }
      }
    }
    this.cd.markForCheck();
    return newJson;
  }
  updateCopySection(json, list) {
    let newJson = this.checkValueChanged(json, list);
    this.autofilljson = {
      leadaddline1: list.leadaddline1,
      leadaddline2: list.leadaddline2,
      leadcountry: list.leadcountry,
      leadstate: list.leadstate,
      leadcity: list.leadcity,
      leadpostcode: list.leadpostcode,
      leadcontactnumber: list.leadcontactnumber,
      leademail: list.leademail,
      isautofill: false,
      projectid: this.projectid,
      leadid: this.leadid,
      createdby: this.userdata.email,
      id: list.id,
      copyjson: newJson
    };
    this.addressService.updateAutoFillDetails(this.autofilljson).subscribe(data => {
      if (data.flag) {
        const index = this.addressList.map((e) => e.id).indexOf(list.id);
        this.addressList[index] = this.autofilljson;
        let emitJson = {
          autofill: '',
          sectionjson: '',
          ischecked: false
        };
        emitJson.autofill = this.autofilljson;
        emitJson.sectionjson = json;
        if ($('#' + json.sectionidf + '_' + list.id).prop('checked')) {
          emitJson.ischecked = true;
          this.copyAnswers.emit(emitJson);
        } else {
          emitJson.ischecked = false;
          this.copyAnswers.emit(emitJson);
        }

        this.divautofillview = true;
        this.divautofilladd = false;
        this.divautofillbutton = false;
        this.cd.markForCheck();
        this.mservice.generateMessage('SUCCESS', 'SUCCESS', data.message);

      } else {
        this.clearAutoFillDetails();
        this.mservice.generateMessage('ERROR', 'FAILED', data.message);
      }
    });
  }
  editAutoFillDetails(list) {
   
    this.autofilljson = {
      leadaddline1: list.leadaddline1,
      leadaddline2: list.leadaddline2,
      leadcountry: list.leadcountry,
      leadstate: list.leadstate,
      leadcity: list.leadcity,
      leadpostcode: list.leadpostcode,
      leadcontactnumber: list.leadcontactnumber,
      leademail: list.leademail,
      isautofill: false,
      projectid: this.projectid,
      leadid: this.leadid,
      createdby: this.userdata.email,
      id: list.id,
      copyjson: list.copyjson
    };
    this.editflag = true;
    this.divautofillview = false;
    this.divautofilladd = true;
    this.divautofillbutton = true;
    this.cd.markForCheck();
  }
  addAutoFillDetail() {
    this.divautofillview = false;
    this.divautofilladd = true;
    this.divautofillbutton = true;
    this.autofilljson = {
      leadaddline1: '',
      leadaddline2: '',
      leadcountry: '',
      leadstate: '',
      leadcity: '',
      leadpostcode: '',
      leadcontactnumber: '',
      leademail: '',
      isautofill: false,
      projectid: this.projectid,
      leadid: this.leadid,
      createdby: this.userdata.email
    };
    this.cd.markForCheck();
  }
  deleteAutoFillDetails(list) {

    this.addressService.deleteAutoFillDetails(list.id).subscribe(data => {
      if (data.flag) {
        this.getAutoFillJson();
        this.cd.markForCheck();
        this.mservice.generateMessage('SUCCESS', 'SUCCESS', data.message);
      } else {
        this.mservice.generateMessage('ERROR', 'FAILED', data.message);
      }
    });
  }
  saveAutoFillDetails() {
    if (this.editflag === true) {
      this.addressService.updateAutoFillDetails(this.autofilljson).subscribe(data => {
        if (data.flag) {
          const index = this.addressList.map((e) => e.id).indexOf(this.autofilljson.id);
          this.addressList[index] = this.autofilljson;
          this.setCheckBoxValues();
          this.divautofillview = true;
          this.divautofilladd = false;
          this.divautofillbutton = false;
          this.cd.markForCheck();
          this.mservice.generateMessage('SUCCESS', 'SUCCESS', data.message);
        } else {
          this.clearAutoFillDetails();
          this.mservice.generateMessage('ERROR', 'FAILED', data.message);
        }
      });
    } else {
      this.addressService.addAutoFillDetails(this.autofilljson).subscribe(data => {
        if (data.flag) {
          this.addressList.push(this.autofilljson);
          this.cd.markForCheck();
          if (data.outdatalist && data.outdatalist != 'No Data Found') {
            this.autofilljson.id = data.outdatalist[0][0].id;
          }
          this.divautofillview = true;
          this.divautofilladd = false;
          this.divautofillbutton = false;
          this.setCheckBoxValues();
          this.mservice.generateMessage('SUCCESS', 'SUCCESS', data.message);
        } else {
          this.clearAutoFillDetails();
          this.mservice.generateMessage('ERROR', 'FAILED', data.message);
        }
      });
    }
  }
  clearAutoFillDetails() {
    this.divautofillview = true;
    this.divautofilladd = false;
    this.divautofillbutton = false;

    this.cd.markForCheck();
    this.setCheckBoxValues();
  }
  public trackByIndex(index: number) {
    return index;
  }

}
