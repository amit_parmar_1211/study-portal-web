import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpBackend, HttpClient } from '@angular/common/http';

let headers = new HttpHeaders();
headers = headers.set('Content-Type', 'application/json; charset=utf-8');
headers = headers.set('notoken', 'true');
@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(
    private httpClient: HttpClient,
    private httpBackend: HttpBackend) {
    this.httpWithoutInterceptor = new HttpClient(httpBackend);
  }
  prefix: string = environment.API_URL + 'api/Assessmentform/';
  private httpWithoutInterceptor: HttpClient;

  public addAutoFillDetails(autofillInfo): Observable<any> {
    return this.httpWithoutInterceptor.post<any>(`${this.prefix}v1/addAutoFillDetails`, autofillInfo, {
      headers
    });
  }
  public getAutoFillDetails(projectid, leadid): Observable<any> {
    return this.httpWithoutInterceptor.get<any>(`${this.prefix}v1/getAutoFillDetails?projectid=${projectid}&leadid=${leadid}`, {
      headers
    });
  }
  public deleteAutoFillDetails(id): Observable<any> {
    return this.httpWithoutInterceptor.get<any>(`${this.prefix}v1/deleteAutoFillDetails?id=${id}`, {
      headers
    });
  }
  public updateAutoFillDetails(autofillInfo): Observable<any> {
    return this.httpWithoutInterceptor.post<any>(`${this.prefix}v1/updateAutoFillDetails`, autofillInfo, {
      headers
    });
  }
}
