import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'splitStringVar'
})
export class SplitStringVarPipe implements PipeTransform {

  transform(value: any, separator: any): any {
    if (value) {
      let splits = value.split(separator);
      let mergerdString = '';
      if (splits.length > 1) {

        for (let i = 0; i < splits.length; i++) {
          if (splits[i].length > 0) {
            mergerdString += splits[i].trim() + ', '
          }
        }
        mergerdString = mergerdString.substring(0, mergerdString.length - 2);
        return [mergerdString];
      } else {
        return splits;
      }
    }

  }

}
@Pipe({
  name: 'formatOutputtemp'
})
export class FormatOutput implements PipeTransform {

  transform(arr: any): any {
    if (arr) {
      if (arr.controltypeidf === 16) {
        let values = arr.controlvalue.split('-');
        let duration = '';
        if (values[0] && values[0].length > 0) {
          duration += '<b>Country Code:</b> ' + values[0];
        }
        if (values[1] && values[1].length > 0) {
          duration += ' <b>Area Code:</b> ' + values[1];
        }
        if (values[2] && values[2].length > 0) {
          duration += ' <b>Phone Number:</b> ' + values[2];
        }
        return duration;
      }

      if (arr.controltypeidf === 17) {
        let values = arr.controlvalue.split('-');
        let duration = '';
        if (values[0] && values[0].length > 0) {
          duration += '<b>Country Code:</b> ' + values[0];
        }
        if (values[1] && values[1].length > 0) {
          duration += ' <b>Phone Number:</b> ' + values[1];
        }
        return duration;
      }

      if (arr.controltypeidf === 38) {
        let values = arr.controlvalue.split('-');
        let duration = '';
        if (values[0] && values[0].length > 0) {
          duration += values[0] + ' years ';
        }
        if (values[1] && values[1].length > 0) {
          duration += values[1] + ' months ';
        }
        if (values[2] && values[2].length > 0) {
          duration += values[2] + ' days';
        }
        return duration;
      }


    }

  }

}
