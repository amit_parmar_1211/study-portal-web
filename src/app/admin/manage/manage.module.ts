import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageRoutingModule } from './manage-routing.module';

import { MstudylevelComponent } from './components/mstudylevel/mstudylevel.component';
import { McurrencyComponent } from './components/mcurrency/mcurrency.component';
import { MdisciplineComponent } from './components/mdiscipline/mdiscipline.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MapCurrencyComponent } from './components/map-currency/map-currency.component';
import { McampuslocationComponent } from './components/mcampuslocation/mcampuslocation.component';
import { UniversityissueComponent } from './components/universityissue/universityissue.component';
import { EdituniversityissueComponent } from './components/edituniversityissue/edituniversityissue.component';
import { MissinguniversityfieldreportComponent } from './components/missinguniversityfieldreport/missinguniversityfieldreport.component';
import { ManagemissingfieldsComponent } from './components/managemissingfields/managemissingfields.component';
import { CourseIssueComponent } from './components/course-issue/course-issue.component'
import { CourseEditComponent } from './components/course-edit/course-edit.component';
@NgModule({
  declarations: [
    MstudylevelComponent,
    McurrencyComponent,
    MdisciplineComponent,
    MapCurrencyComponent,
    McampuslocationComponent,
    UniversityissueComponent,
    EdituniversityissueComponent,
    MissinguniversityfieldreportComponent,
    ManagemissingfieldsComponent,
    CourseIssueComponent,
    CourseEditComponent
  ],
  imports: [
    CommonModule,
    ManageRoutingModule,
    SharedModule
  ]
})
export class ManageModule { }
